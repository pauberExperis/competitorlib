﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collection
{
    public class Competitor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string Nationality { get; set; }
        public string Role { get; set; }
        public int OrganisationId { get; set; }
        public Organisation Organisation { get; set; }
        public ICollection<CompetitorSkill> CompetitorSkills { get; set; }


        public Competitor(string name, int age, string nationality, string role, int organisationId)
        {
            Name = name;
            Age = age;
            Nationality = nationality;
            Role = role;
            OrganisationId = organisationId;            
        }

    }

    
}
