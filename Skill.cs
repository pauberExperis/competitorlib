﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collection
{
    public class Skill
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection <CompetitorSkill> CompetitorSkills { get; set; }


        public Skill(string name)
        {
            Name = name;
        }
    }
}
