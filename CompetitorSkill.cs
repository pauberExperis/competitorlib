﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collection
{
    public class CompetitorSkill
    {
        public int CompetitorId { get; set; }
        public Competitor Competitor { get; set; }
        public int SkillId { get; set; }
        public Skill Skill { get; set; }
    }
}
