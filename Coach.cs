﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collection
{
    public class Coach
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public string Nationality { get; set; }
        public int OrganisationId { get; set; }
        public Organisation Organisation { get; set; }
     

        public Coach(string name, int age, string nationality,  int organisationId)
        {
            Name = name;
            Age = age;
            Nationality = nationality;
            OrganisationId= organisationId;
        }
    }
}
