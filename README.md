# competitor-lib

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![DLL](https://raster.shields.io/badge/Competitor-Form-informational?style=flat-square)](https://gitlab.com/pauberExperis/competitoref)
> Competitor Library for Entity Framework Assignment

## Table of Contents

- [Run](#run)
- [Maintainers](#maintainers)
- [Components](#components)
- [Contributing](#contributing)
- [License](#license)

## Run
```
Visual Studio Run
```
## Components

Competitor Class Library for Competitor Entity Framework Assignment, linked in badge

## Maintainers

[Philip Aubert (@pauberexperis)](https://gitlab.com/pauberexperis)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

Experis © 2020 Noroff Accelerate AS