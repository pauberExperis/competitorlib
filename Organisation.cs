﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collection
{
    public class Organisation
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Region { get; set; }

        public Coach Coach { get; set; }
        public ICollection<Competitor> Competitors { get; set; }

        public Organisation(string name, string region)
        {
            Name = name;
            Region = region;
        }
    }
}
